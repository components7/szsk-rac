// 图片裁切的Modal

import { Modal } from 'antd';
import React, { useState } from 'react';
import { Cropper } from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import './index.scss';

declare type IProps = {
  pic: any;
  visible: boolean; // 控制Modal显示
  closeModal: () => void; // 关闭Modal
  onCropData: (data: any) => void; // // 将数据传到父组件
  isSumbit?: boolean;
};

const CropPicModal = (props: IProps) => {
  const { visible, pic, isSumbit } = props;
  // const [cropData, setCropData] = useState('#'); // 这个是裁切后的图片数据 返给父组件
  const [cropper, setCropper] = useState<any>();
  // cropData;
  const getCropData = () => {
    if (typeof cropper !== 'undefined') {
      const data = cropper.getCroppedCanvas().toDataURL();
      // setCropData(data);
      props.onCropData(data); // 裁切的那张图片的数据
      props.closeModal();
    }
  };
  const onModalOk = () => {
    // 直接走检索了
    getCropData();
  };
  const getPic = () => {
    const a = pic && pic.split('//')[1].split('/');
    if (!a) { return null }
    return `/${a[1]}/${a[2]}`;
  };
  return (
    <Modal
      title="图片裁切"
      visible={visible}
      onOk={onModalOk}
      okText="检索"
      onCancel={() => {
        props.closeModal();
      }}
      maskClosable={false}
      width={780}
      centered
      destroyOnClose
      className="m-myInfo-modal"
    >
      <div style={{ display: 'flex', alignItems: 'item', justifyContent: 'center' }}>
        <div style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <br />
          <br />
          <Cropper
            style={{ height: 400, width: '100%' }}
            zoomTo={0}
            initialAspectRatio={1}
            preview=".img-preview"
            src={isSumbit ? getPic() : pic}
            viewMode={1}
            guides
            autoCrop={false}
            minCropBoxHeight={10}
            minCropBoxWidth={10}
            background={false}
            responsive
            autoCropArea={1}
            checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
            onInitialized={(instance) => {
              setCropper(instance);
            }}
          />
        </div>
        <br style={{ clear: 'both' }} />
      </div>
    </Modal>
  );
};
export default CropPicModal;
