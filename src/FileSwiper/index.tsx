import React, { useState, useEffect } from 'react';
import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import useResizeAware from 'react-resize-aware';
import ContainAttach from '../ContainAttach';

import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import './index.scss';

SwiperCore.use([Navigation]);

interface IProps {
  perWidth: string; // 每个轮播图的宽度,px
  perHeight: string; // 每个轮播图的高度
  list: IAttachmentItem[]; // 附件列表
  picCallback?: (i?: number)=>void;
  videoCallback?: (i?: number, dataUrl?: string)=>void;
  cropEnable?: boolean; // 视频是否支持截图
}

type IAttachmentItem = {
  caseId?:string;
  attachmentId?:string;
  user?:number;
  path?:string;
  name?:string;
  contentType?:string;
  size?:number;
  fileType?:number;
  finishModeled?: number;
  abstractPath?: string; // 已剪裁图片路径
}

const FileSwiper = (props: IProps) => {
  const { picCallback, list = [], perWidth = '100px', perHeight = '100px', cropEnable, videoCallback } = props;
  const [PerViewNum, setPerViewNum] = useState(0);
  const [containerResizeListener, containerSizes] = useResizeAware();
  useEffect(() => {
    // 78为两侧padding
    const contentWidth = (containerSizes.width || 0) - 78;
    // 1为每个块的最小间距
    const num = Math.floor(contentWidth / (+(perWidth.replace('px', '')) + 1))
    setPerViewNum(num || 1)
  }, [containerSizes, perWidth])

  return (
    <div className="m-component-file-swiper">
      {containerResizeListener}
      {PerViewNum > 0 && (
        <Swiper
          className="swiper-no-swiping"
          observer
          slidesPerView={PerViewNum}
          navigation
          initialSlide={0}
        >
          {list.map((item: IAttachmentItem, index) => (
            <SwiperSlide>
              <ContainAttach
                cropEnable={cropEnable}
                width={perWidth}
                height={perHeight}
                outerClass="f-center"
                fileSrc={item.abstractPath || item.path || ''}
                contentType={item.contentType || ''}
                finishModeled={item.finishModeled} // 案件建模中需要
                picCallback={picCallback ? () => { picCallback(index) } : undefined}
                videoCallback={videoCallback ? (dataUrl?: string) => { videoCallback(index, dataUrl) } : undefined}
              />
            </SwiperSlide>
          ))}
        </Swiper>
      )}
    </div>
  );
};

export default FileSwiper;
