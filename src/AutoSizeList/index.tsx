import React, { useEffect, useState } from 'react';
import styles from  './index.scss'
import useResizeAware from 'react-resize-aware';

interface IProps {
  // 数据源
  list: any[];
  // item渲染方式
  render: (item: any, index?: number) => JSX.Element;
  // item宽度
  itemWidth: number;
  // 两个item中间最小padding
  minSpace?: number;
}
const AutoSizeList = (props: IProps) => {
  const { list = [], render, itemWidth, minSpace = 10 } = props
  const [numObj, setNumObj] = useState({ itemNum: 1, space: minSpace });
  const [containerResizeListener, containerSizes] = useResizeAware();
  const { itemNum: rowCount, space } = numObj;

  useEffect(() => {
    if (containerSizes.width) {
      const itemNum = Math.floor(containerSizes.width / (itemWidth + minSpace * 2))
      const mySpace = Math.floor((containerSizes.width - itemNum * itemWidth) / (itemNum * 2) - 1)
      setNumObj({ itemNum, space: mySpace });
    }
  }, [containerSizes.width]);

  return (
    <div className={styles.autoSizeList}>
      {containerResizeListener}
      {
        list.map((item, idx) => {
          if (idx % rowCount === 0) {
            return (
              <div className={styles.row}>
                {
                  list.map((_item, jdx) => {
                    if (jdx >= idx && jdx < idx + rowCount) {
                      return (
                        <div className={styles.autoSpaceItem} style={{ margin: `0 ${space}px 0 ${space}px` }}>
                          {render(_item, jdx)}
                        </div>
                      )
                    }
                    return null;
                  })
                }
              </div>
            )
          }
          return null;
        })
      }
    </div>
  )
}

export default AutoSizeList;
