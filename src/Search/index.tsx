import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import { Button } from 'antd';
import SForm, { ISFormProps } from '../Form';
import styles from './index.scss';
import { objUtils } from '@szsk/utils';

interface ISSearch extends ISFormProps {
  // 搜索回调
  onSearch: (params: any, toFirst: boolean, isReset: boolean) => any;
}

// forwardRef的意义需要探究一下 todo
const SSearch = forwardRef((props: ISSearch, ref) => {
  const { onSearch, formItems, columns = 4 } = props;
  const formRef = useRef<any>(null);

  useEffect(() => {
    // 进入则搜索一次
    search(true);
  }, []);

  // 搜索
  const search: (isReset?: boolean) => void = (isReset?: boolean) => {
    formRef.current!.validateFields().then((values: any) => {
      if (onSearch) {
        const _values = objUtils.removeNull(values);
        onSearch(_values, true, !!isReset);
      }
    });
  };

  // 这里似乎没用
  useImperativeHandle(ref, () => ({
    form: formRef,
  }));

  // 重置
  const reset: () => void = () => {
    formRef.current!.resetFields();
    search(true);
  };
  return (
    <div className={styles.search}>
      <SForm ref={formRef} formItems={formItems} columns={columns} />
      {/* 这里没有适配一行放满的情况 ${
          formItems.length % columns === 0 ? styles.fNewLine : ''
        } */}
      <div className={`${styles.btns}`}>
        <Button onClick={() => reset()}>重置</Button>
        <Button
          onClick={() => {
            search();
          }}
          type="primary"
          className={styles.searchBtn}
        >
          查询
        </Button>
      </div>
    </div>
  );
});

export default SSearch;
