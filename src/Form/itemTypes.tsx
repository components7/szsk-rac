import React from 'react';
import { Input, Select, Radio, Checkbox, DatePicker } from 'antd';
import {
  IFormCheckbox,
  IFormDatePicker,
  IFormInput,
  IFormRadio,
  IFormSelect,
  IFormType,
  ISelectOption,
} from './index.d';

export default function getItemComponent(type: IFormType) {
  // input 类型
  const input = (item: IFormInput) => {
    const { props = {} } = item;
    return (
      <Input
        {...props}
        placeholder={props.placeholder || `请输入${item.label}`}
      />
    );
  };

  // select 类型
  const select = (item: IFormSelect) => {
    const { options, props = {} } = item;
    return (
      <Select
        placeholder={props.placeholder || `请选择${item.label}`}
        {...props}
      >
        {options.map((option: ISelectOption) => {
          const { label, value } = option;
          return (
            <Select.Option key={value} value={value}>
              {label}
            </Select.Option>
          );
        })}
      </Select>
    );
  };

  // radio类型
  const radio = (item: IFormRadio) => {
    const { options, props = {} } = item;
    return <Radio.Group options={options} {...props} />;
  };

  // checkbox类型
  const checkbox = (item: IFormCheckbox) => {
    const { options, props = {} } = item;
    return <Checkbox.Group {...props} options={options} />;
  };

  // datepicker类型
  const datePicker = (item: IFormDatePicker) => {
    const { props = {} } = item;
    return <DatePicker {...props} />;
  };
  return {
    input,
    select,
    radio,
    checkbox,
    datePicker,
  }[type];
}
