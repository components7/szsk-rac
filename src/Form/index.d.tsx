import { InputProps } from 'antd/lib/input';
import { SelectProps } from 'antd/lib/select';
import { RadioGroupProps } from 'antd/lib/radio';
import { CheckboxGroupProps } from 'antd/lib/checkbox';
import { DatePickerProps } from 'antd/lib/date-picker';

// 基础类型
export type IFormItem =
  | IFormInput
  | IFormSelect
  | IFormRadio
  | IFormDatePicker
  | IFormCheckbox;

export type IFormType =
  | 'input'
  | 'select'
  | 'radio'
  | 'datePicker'
  | 'checkbox';

export interface IFormItemBase {
  // 输出属性key，id
  id: string;
  // 显示名称
  label: string;
  // 可选表单类型
  type?: IFormType;
  // 自定义组件
  render?: any;
  // 初始值
  initialValue?: any;
}

export interface IFormInput extends IFormItemBase {
  props?: InputProps;
}

export interface IFormSelect extends IFormItemBase {
  props?: SelectProps<any>;
  // 可选列表
  options: ISelectOption[];
}

export interface ISelectOption {
  label: string;
  value: string | number;
}

export interface IFormRadio extends IFormItemBase {
  props?: RadioGroupProps;
  // 可选列表
  options: ISelectOption[];
}

export interface IFormDatePicker extends IFormItemBase {
  props?: DatePickerProps;
}

export interface IFormCheckbox extends IFormItemBase {
  props?: CheckboxGroupProps;
  // 可选列表
  options: ISelectOption[];
}
