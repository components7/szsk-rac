import React from 'react';
import { Tooltip } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import './index.scss'

interface IProps {
  title: string
}

const TextCopy = (props: IProps) => {
  const { title } = props

  const renderTooltipCopy = () => (
    <>
      {title}
      <CopyToClipboard
        text={title}
      >
        <span className="m-copy">复制</span>
      </CopyToClipboard>
    </>
  )

  return (
    <Tooltip
      className="m-title"
      placement="bottom"
      title={renderTooltipCopy()}
    >
      <span>{title}</span>
    </Tooltip>
  )
}

export default TextCopy;
