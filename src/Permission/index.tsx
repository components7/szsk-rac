import React from 'react'
import { useSelector } from 'react-redux'
import { permissionUtils } from '@szsk/utils'

interface IPageProps {
  children: JSX.Element | JSX.Element[];
  somePermissions?: string[];
}

const Permission = (props: IPageProps) => {
  const { children, somePermissions = [] } = props
  const permissionList = useSelector((state: any) => state.user.permissionList);
  return (
    <>
      {permissionUtils.somePermissionAllow(somePermissions, permissionList) ? children : ''}
    </>
  )
}

export default Permission
