# 使用
`npm install @szsk/rac`

## 使用范围
依赖antd，antd的补充，react后台管理

## 已有组件
### Form
表单配置化，不用写那么繁杂的form
### Search
search配置化
### ContentWrapper
通用容器卡片
### Permission
权限控制容器
### AutoSizeList
自适应容器下用于列表自适应排列
### ContainImage
图片自适应块居中展示
### ContainVideo
video自适应块居中展示
### ContainAttach
图片或video自适应块居中展示，根据type自动区分
### FileSwiper
attach轮播
### PictrueJigsaw
多图片拼图
### TextCopy
复制功能

## 后续安排
1.组件文档开发
2.组件完善测试
3.组件拓展